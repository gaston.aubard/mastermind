"""
Version 2 du codemaker, se base sur l'algo d'évaluation de common.
Contient codemaker. Permet de tricher tant qu'on ne le voit pas.
"""

import sys
import common
import codebreaker2


def init():
    """
    Cette fonction, appellée à chaque début de partie, initialise un certain nombre de
    variables utilisées par le codemaker
    """
    global solution
    global possibles
    global dictionnaire
    dictionnaire = {}
    possibles = {None}
    solution = ''.join(common.choices(common.COLORS, common.LENGTH))
    tout = common.enum(common.COLORS, common.LENGTH)
    for i in tout:
        possibles.add(tuple(i))
    possibles.remove(None)


def eval_possibles(attempt):
    global possibles
    global dictionnaire
    dictionnaire = {}
    if len(possibles) > 500:
            print("patience, cela peut prendre un moment")
    for i in possibles:
        score = common.evaluation(attempt, i)
        temp_possibles = possibles.copy()
        common.maj_possibles(temp_possibles, attempt, score)
        dictionnaire[i] = len(temp_possibles)


def codemaker(attempt):
    """
    Cette fonction corrige la combinaison proposée par le codebreaker
    (donnée en argument).
    Elle triche aussi.
    """
    global dictionnaire
    global possibles
    global solution
    eval_possibles(attempt)
    maxval = max(dictionnaire.values())
    res = [k for k, v in dictionnaire.items() if v==maxval]
    solution = res[0]
    evaluation_p = common.evaluation(solution, attempt)
    common.maj_possibles(possibles, attempt, evaluation_p)
    return evaluation_p
