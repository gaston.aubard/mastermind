import common
import codebreaker2
import codemaker1
import matplotlib.pyplot as plt


def play_q7(codemaker, codebreaker):
    """
    Fait jouer le codemaker et le codebreaker et renvoie le nombre d'essais.
    """
    n_tries = 0
    codebreaker.init()
    codemaker.init()
    evaluation_p = None
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            codebreaker.remises = []
            return n_tries


# Ci-dessous l'affichage du graphe des résultats.


names = []
values = []
i = 0
while i < 100:
    names.append(str(i+1))
    values.append(play_q7(codemaker1, codebreaker2))
    i += 1

plt.bar(names, values)
plt.show()
