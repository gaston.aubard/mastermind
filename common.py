"""
Contient les programmes d'évaluation et d'énumération des possibles.
Contient aussi l'algo de tirage au sort de combinaisons.
Noms des programmes : choices, evaluation, donner_possibles.
"""

import random
import sys

LENGTH = 3
COLORS = ['R', 'V', 'B', 'J', 'N', 'M', 'O', 'G']


def choices(e, n):
    """Renvoie une liste composée de n éléments tirés de e avec remise
    On pourrait utiliser random.choices, mais cette fonction n'est pas
    disponible dans les versions plus anciennes de Python
    """
    return [random.choice(e) for i in range(n)]


def evaluation(attempt, solution):
    '''
    Compare attempt et solution. Retourne les valeurs de (red, white).
    Attempt et solution sous forme 'RRRR', ('R', 'R', 'R', 'R') ou
    ['R', 'R', 'R', 'R']
    '''
    if len(solution) != len(attempt):
        sys.exit("Erreur : les deux combinaisons n'ont pas la même longueur")
    attempt = list(attempt)
    solution = list(solution)
    red = 0
    white = 0
    sol = solution.copy()
    att = attempt.copy()
    for i in range(len(attempt)):
        if attempt[i] == solution[i]:
            red = red + 1
            sol[i] = 0
            att[i] = 1
    sol = set(sol)
    for i in att:
        if i in sol:
            white += 1
            sol.remove(i)
    return(red, white)


def donner_possibles(combinaison, eval):
    '''
    Donne les différentes combinaisons possibles à paratir d'une combinaison et de son
    évaluation.
    Sortie : set de tuples de couleurs.
    '''
    possibles = {None}
    tout = enum(COLORS, LENGTH)
    for i in tout:
        if evaluation(i, combinaison) == eval:
            possibles.add(tuple(i))
    possibles.remove(None)
    return possibles


def maj_possibles(possibles, combinaison, eval):
    '''
    Met à jour les combinaisons possibles.
    Le premier argument est le set des possibles avant.
    Le set des possibles est directement modifié.
    '''
    temp = {None}
    for i in possibles:
        if evaluation(i, combinaison) != eval:
            temp.add(i)
    temp.remove(None)
    for j in temp:
        possibles.remove(j)
    return


def enum(colors, length):
    '''
    Cette fonction énumère les différentes combinaisons
    ayant pour taille length et avec les caractères de colors.
    Sortie : un tableau de tableaux de suites de couleurs.
    Exemple : [['A', 'B'], ['A', 'A'], ...]
    '''
    result = []
    if length == 1:
        for j in colors:
            a = []
            a.append(j)
            result.append(a)
        return result
    else:
        a = enum(colors, length-1)
        for i in a:
            for j in colors:
                b = i.copy()
                b.insert(0, j)
                result.append(b)
    return result
