"""
Version 2 du codebreaker.
Tirage sans remise et utilisation de donner_possibles et maj_possibles.
Contient codebreaker.
"""


import common


def init():
    """
    Une fonction qui ne fait rien... pour cette version triviale.
    Pour vos codebreaker plus avancés, c'est ici que vous pouvez initialiser
    un certain nombre de variables à chaque début de partie.
    """
    return


remises = []
attempt = []
possibles = {None}
count = 0


def codebreaker(evaluation_p):
    """
    Teste une première combinaison aléatoire.
    Ensuite, teste d'autres combinaisons compatibles avec l'argument evaluation_p.
    """
    global remises
    global possibles
    global attempt
    global count

    if evaluation_p == None:  # Premier tour : aléatoire
        while True:
            attempt = common.choices(common.COLORS, common.LENGTH)
            remises.append(attempt)
            count = 1
            return ''.join(attempt)

    if count == 1:  # Deuxième tour : génère une liste des possibles avec la première évaluation
        possibles = common.donner_possibles(attempt, evaluation_p)
        count = 2

    if evaluation_p != None:  # Deuxième tour et suivants : Tirage sans remise compatible avec possibles
        common.maj_possibles(possibles, attempt, evaluation_p)
        while True:
            attempt = common.choices(common.COLORS, common.LENGTH)
            copie = tuple(attempt.copy())
            if copie in possibles:
                if attempt not in remises:
                    remises.append(attempt)
                    return ''.join(attempt)
