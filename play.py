"""
Interface utilisateur.
Contient play et play_human_against...
"""

import common


def play(codemaker, codebreaker):
    """
    Fonction principale de ce programme :
    Fait jouer ensemble le codebreaker et le codemaker donnés en arguments
    """
    n_tries = 0
    codebreaker.init()
    codemaker.init()
    evaluation_p = None
    print('Combinaisons de taille {}, couleurs disponibles {}'.format(common.LENGTH, common.COLORS))
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        print("Essai {} : {} ({},{})".format(n_tries, attempt, red, white))
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            print("Bravo ! Trouvé {} en {} essais".format(attempt, n_tries))
            break


def play_log(codemaker, codebreaker, log):
    """
    Fonction principale de ce programme :
    Fait jouer ensemble le codebreaker et le codemaker donnés en arguments.
    Enregistre un log dans le fichier texte donné en dernier argument.
    """
    f = open(log, "w")
    n_tries = 0
    codebreaker.init()
    codemaker.init()
    evaluation_p = None
    print('Combinaisons de taille {}, couleurs disponibles {}'.format(common.LENGTH, common.COLORS))
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        print("Essai {} : {} ({},{})".format(n_tries, attempt, red, white))
        evaluation_p = (red, white)
        f.write("{}\n{},{}\n".format(attempt, red, white))
        if red >= common.LENGTH:
            print("Bravo ! Trouvé {} en {} essais".format(attempt, n_tries))
            f.close()
            break


def play_human_against_codemaker(codemaker):
    """
    Fait jouer l'utilisateur humain (au clavier) dans le role du codebreaker
    contre un codemaker donné en argument
    """
    n_tries = 0
    codemaker.init()
    evaluation_p = None
    print('Combinaisons de taille {}, couleurs disponibles {}'.format(common.LENGTH, common.COLORS))
    while True:
        attempt = input("Saisir combinaison: ")  # On lit une combinaison au clavier au lieu d'appeler le codebreaker (qui sera donc joué par un humain)
        if len(attempt) != 4:
            print("Combinaison invalide (pas la bonne taille)")
            continue
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        print("Essai {} : {} ({},{})".format(n_tries, attempt, red, white))
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            print("Bravo ! Trouvé {} en {} essais".format(attempt, n_tries))
            break


def play_human_against_codebreaker(codebreaker):
    """
    Fait jouer l'utilisateur humain (au clavier) dans le role du codemaker
    contre un codebreaker donné en argument
    """
    n_tries = 0
    codebreaker.init()
    evaluation_p = None
    print('Combinaisons de taille {}, couleurs disponibles {}'.format(common.LENGTH, common.COLORS))
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        print('Combinaison proposée: {}'.format(attempt))
        red = int(input('Saisir nombre de plots rouges: '))
        white = int(input('Saisir nombre de plots blancs: '))
        n_tries += 1
        print("Essai {} : {} ({},{})".format(n_tries, attempt, red, white))
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            print("Le codebreaker a trouvé {} en {} essais".format(attempt, n_tries))
            break


def check_codemaker(log):
    f = open(log, "r")
    length = int(f.readlines()[-1][0])
    if length != common.LENGTH:
        print("La combinaison n'a pas été trouvée")
        f.close()
        return
    f.close()
    f = open(log, "r")
    iterate = (file_len(log) // 2) - 1
    solution = str(f.readlines()[-2])
    i = 0
    f = open(log, "r")
    lines = (f.readlines())
    f.close()
    for i in range(iterate):
        evaluation = int(lines[2*i+1].split(",")[0]), int(lines[2*i+1].split(",")[1])
        if common.evaluation(lines[2*i][0:(len(lines[2*i])-1)], solution[0:(len(solution)-1)]) != evaluation:
            print("Il y a eu triche ! >:(")
            return
    print("Tout bon, pas de triche évidente :D")
    return


def file_len(file):
    with open(file) as f:
        for i, a in enumerate(f):
            pass
    return i + 1


def masterplay(codemaker, codebreaker, log):
    if codemaker == None:
        play_human_against_codebreaker(codebreaker)
    if codebreaker == None:
        play_human_against_codemaker(codemaker)
    else:
        if log == None:
            play(codemaker, codebreaker)
        else:
            play_log(codemaker, codebreaker, log)


import codemaker0
import codemaker1
import codemaker2
import codebreaker0
import codebreaker1
import codebreaker2

log = "file.txt"
masterplay(codemaker2, codebreaker2, log)
check_codemaker(log)

