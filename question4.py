import common
import codebreaker1
import codemaker1
import matplotlib.pyplot as plt


def play_q4(codemaker, codebreaker):
    """
    Fait jouer le codemaker et le codebreaker et renvoie le nombre d'essais.
    """
    n_tries = 0
    codebreaker.init()
    codemaker.init()
    evaluation_p = None
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            codebreaker1.remises = []
            return n_tries


def calcul_esp(tours):
    """
    Pour un nombre de tours donné, renvoie la moyenne du nombre d'essais.
    """
    n = 1
    esptot = 0
    while(n <= tours):
        esptot = esptot + play_q4(codemaker1, codebreaker1)
        esp = esptot / n
        print(esp)
        n += 1
    return


# Ci-dessous l'affichage du graphe des résultats.


names = []
values = []
i = 0
while i < 100:
    names.append(str(i+1))
    values.append(play_q4(codemaker1, codebreaker1))
    i += 1

plt.bar(names, values)
plt.show()


'''
Espérance : 2048.5
'''
