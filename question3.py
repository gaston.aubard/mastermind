import common
import codebreaker0
import codemaker1
import matplotlib.pyplot as plt


def play_q3(codemaker, codebreaker):
    '''
    Fait jouer le codemaker et le codebreaker, retourne le nombre d'essais nécessaires.
    '''
    n_tries = 0
    codebreaker.init()
    codemaker.init()
    evaluation_p = None
    while True:
        attempt = codebreaker.codebreaker(evaluation_p)
        (red, white) = codemaker.codemaker(attempt)
        n_tries += 1
        evaluation_p = (red, white)
        if red >= common.LENGTH:
            return n_tries


def calcul_esp(tours):
    '''
    Calcule la moyenne des essais nécessaires sur un nombre de tours en argument.
    '''
    n = 1
    esptot = 0
    while(n <= tours):
        esptot = esptot + play_q3(codemaker1, codebreaker0)
        esp = esptot / n
        print(esp)
        n += 1
    return

# Ci-dessous, affichage du graphe des essais.

names = []
values = []
i = 0
while i < 1000:
    names.append(str(i+1))
    values.append(play_q3(codemaker1, codebreaker0))
    i += 1

plt.bar(names, values)
plt.show()

'''
X variable qui compte le premier succès
X suit une loi géométrique
Donc espérance de 4096
'''
