"""
Version 1 du codebreaker. Teste des programmes mais pas deux fois
le même. Contient la fonction codebreaker.
"""


import common


def init():
    """
    Une fonction qui ne fait rien... pour cette version triviale.
    Pour vos codebreaker plus avancés, c'est ici que vous pouvez initialiser
    un certain nombre de variables à chaque début de partie.
    """
    return


remises = []


def codebreaker(evaluation_p):
    """
    Cette fonction n'utilise pas l'argument evaluation_p.
    Elle teste des combinaisons qui ne sont pas dans global remises et
    ajoute à remises la combinaison testée.
    """
    global remises
    while True:
        a = common.choices(common.COLORS, common.LENGTH)
        if a not in remises:
            remises.append(a)
            return ''.join(a)
